package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.EgressDto;
import com.practica.empresarial.veterinaria.service.EgressServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class EgressControllerTest {

    EgressController egressController;
    EgressServices egressServices;

    EgressDto sampleEgressUpdateDto;
    EgressDto sampleEgressCreateDto;
    private List<EgressDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        egressServices = mock(EgressServices.class);
        egressController = new EgressController(egressServices);

        sampleEgressUpdateDto = EgressDto.builder()
                .idEgress(1L)
                .date(new Date())
                .concept("antibioticos oral")
                .idUser(1L)
                .idPet(1L)
                .description("Compra de medicamento")
                .value(50f)
                .type("medicamento")
                .build();

        sampleEgressCreateDto = EgressDto.builder()
                .idEgress(null)
                .date(new Date())
                .concept("antibioticos oral")
                .idUser(1L)
                .idPet(1L)
                .description("Compra de medicamento")
                .value(15000f)
                .type("medicamento")
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleEgressUpdateDto);

    }

    @Test
    void testControllerCreateEgress() {
        when(egressServices.createEgress(Mockito.any(EgressDto.class))).thenReturn(sampleEgressCreateDto);
        EgressDto actualAnswer = egressController.createEgress(sampleEgressCreateDto);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void testControllerUpdateEgress() {
        when(egressServices.updateEgress(Mockito.any(EgressDto.class))).thenReturn(sampleEgressUpdateDto);
        EgressDto actualAnswer = egressController.updateEgress(sampleEgressCreateDto);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void testControllerGetAll() {
        when(egressServices.getAllEgress()).thenReturn(sampleDataTest);
        List<EgressDto> egress = egressController.getAllEgress();
        when(egressServices.getAllEgress()).thenReturn(sampleDataTest);
        assertThat(egress).isNotEmpty();
    }

    @Test
    void testControllerGetAllType() {
        when(egressServices.getAllEgressByType(sampleEgressUpdateDto.getType())).thenReturn(sampleDataTest);
        List<EgressDto> egress = egressController.getAllEgressByType("medicamento");
        assertThat(egress).isNotEmpty();
    }

    @Test
    void testControllerGetAllPet() {
        when(egressServices.getAllEgressByPet(sampleEgressUpdateDto.getIdPet())).thenReturn(sampleDataTest);
        List<EgressDto> egress = egressController.getAllEgressByPet(1L);
        assertThat(egress).isNotEmpty();
    }
}