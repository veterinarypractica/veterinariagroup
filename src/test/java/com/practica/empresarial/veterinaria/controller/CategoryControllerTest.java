package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.factorie.CategoryFactory;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;
import com.practica.empresarial.veterinaria.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CategoryControllerTest {

    CategoryController categoryController;
    CategoryService categoryService;

    CategoryDto sampleCategoryUpdateDto;
    CategoryDto sampleCategoryCreateDto;
    CategoryDto sampleCategory;

    @BeforeEach
    public void setup() {
        categoryService = mock(CategoryService.class);
        categoryController = new CategoryController(categoryService);
        sampleCategoryUpdateDto = new CategoryFactory().newInstanceDto();
        sampleCategoryCreateDto = new CategoryFactory().setIdCategory(null).newInstanceDto();
        sampleCategory = new CategoryFactory().newInstanceDto();
    }


    @Test
    void createCategory() {
        when(categoryService.createCategory(Mockito.any(CategoryDto.class))).thenReturn(sampleCategory);
        CategoryDto actualCategory = categoryController.createCategory(sampleCategoryCreateDto);
        assertThat(actualCategory).isNotNull();
    }

    @Test
    void updateCategory() {
        when(categoryService.updateCategory(Mockito.any(CategoryDto.class))).thenReturn(sampleCategory);
        CategoryDto actualCategory = categoryController.updateCategory(sampleCategoryUpdateDto);
        assertThat(actualCategory).isNotNull();
    }


    @Test
    void getOneByName() {
        when(categoryService.getOneByName("Dolby")).thenReturn(sampleCategory);
        ResponseEntity<CategoryDto> category = categoryController.getOneByName("Dolby");
        assertThat(category).isNotNull();
    }
}