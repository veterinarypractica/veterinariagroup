package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.factorie.PetFactory;
import com.practica.empresarial.veterinaria.model.dto.PetDto;
import com.practica.empresarial.veterinaria.service.PetServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PetControllerTest {

    private PetController petController;
    private PetServices petServices;

    private PetDto samplePetUpdateDto;
    private List<PetDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        petServices = mock(PetServices.class);
        petController = new PetController(petServices);

        samplePetUpdateDto = new PetFactory().newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(samplePetUpdateDto);
    }

    @Test
    void testControllerGetAllByUser() {
        when(petServices.getAllPetByUser(Mockito.anyString())).thenReturn(sampleDataTest);
        List<PetDto> pet = petController.getAllPetByUser("1");
        assertThat(pet).isNotEmpty();
    }
}
