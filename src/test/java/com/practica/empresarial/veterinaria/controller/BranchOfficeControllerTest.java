package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;
import com.practica.empresarial.veterinaria.service.BranchOfficeServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class BranchOfficeControllerTest {

    BranchOfficeController branchOfficeController;
    BranchOfficeServices branchOfficeServices;

    BranchOfficeDto sampleBranchOfficeUpdateDto;
    BranchOfficeDto sampleBranchOfficeCreateDto;
    private List<BranchOfficeDto> sampleDataTest;

    @BeforeEach
    public void setup() {
        branchOfficeServices = mock(BranchOfficeServices.class);
        branchOfficeController = new BranchOfficeController(branchOfficeServices);

        sampleBranchOfficeUpdateDto = BranchOfficeDto.builder()
                .idBranchOffice(1L)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();

        sampleBranchOfficeCreateDto = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();

        sampleDataTest = new ArrayList<>();
        sampleDataTest.add(sampleBranchOfficeUpdateDto);

    }

    @Test
    void createBranchOffice() {
        when(branchOfficeServices.createBranchOffice(Mockito.any(BranchOfficeDto.class))).thenReturn(sampleBranchOfficeUpdateDto);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(null)
                .address("street11")
                .branchPhone(3123196836L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        BranchOfficeDto actualMessage = branchOfficeController.createBranchOffice(BranchOffice);
        assertThat(actualMessage).isNotNull();
    }

    @Test
    void updateBranchOffice() {
        when(branchOfficeServices.updateBranchOffice(Mockito.any(BranchOfficeDto.class))).thenReturn(sampleBranchOfficeUpdateDto);
        BranchOfficeDto BranchOffice = BranchOfficeDto.builder()
                .idBranchOffice(2L)
                .address("street12")
                .branchPhone(312319786L)
                .logo("logo.png")
                .admin(1144201661L)
                .idVeterinary(9L)
                .build();
        BranchOfficeDto actualMessage = branchOfficeController.updateBranchOffice(BranchOffice);
        assertThat(actualMessage).isNotNull();
    }

    @Test
    void getAll() {
        when(branchOfficeServices.getAllBranchOffice()).thenReturn(sampleDataTest);
        List<BranchOfficeDto> BranchOffices = branchOfficeController.getAll();
        int actualSize = BranchOffices.size();
        assertTrue(actualSize > 0);
    }

    @Test
    void deleteById() {
        when(branchOfficeServices.deleteBranchOffice(1L)).thenReturn("Se ha eliminado exisitosamente");
        String actualMessage = branchOfficeController.deleteById(1L);
        String expectedMessage = "Se ha eliminado exisitosamente";
        assertTrue(actualMessage.contains(expectedMessage));
    }
}