package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.factorie.CardVaccineFactory;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;
import com.practica.empresarial.veterinaria.service.CardVaccineServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class CardVaccineControllerTest {

    CardVaccineController cardVaccineController;
    CardVaccineServices cardVaccineServices;

    CardVaccineDto sampleCardVaccine;
    Map<String, Object> data;

    @BeforeEach
    public void setup() {

        cardVaccineServices = mock(CardVaccineServices.class);
        cardVaccineController = new CardVaccineController(cardVaccineServices);
        sampleCardVaccine = new CardVaccineFactory().newInstanceDto();
        data = new HashMap<>();
    }

    @Test
    void testControllerSendEmail() {
        when(cardVaccineServices.sendByEmailCardVaccine(Mockito.anyString(), Mockito.anyInt())).thenReturn("string");
        String cadVaccine = cardVaccineController.sendMailPDF("sample@email.com", 1);
        assertThat(cadVaccine).isNotBlank();
    }

    @Test
    void testControllerGetOneByIdPet() {
        when(cardVaccineServices.getOneByIdPet(Mockito.anyInt())).thenReturn(sampleCardVaccine);
        CardVaccineDto cardVaccine = cardVaccineController.getOneByIdPet(1);
        assertThat(cardVaccine).isNotNull();
    }

    @Test
    void testControllerGetCardVaccine() {
        when(cardVaccineServices.getCardVaccine(Mockito.anyInt())).thenReturn(data);
        Map<String, Object> actual = cardVaccineController.getCardVaccine(1);
        assertThat(actual).isNotNull();
    }

}