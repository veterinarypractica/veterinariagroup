package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.factorie.IncomeFactory;
import com.practica.empresarial.veterinaria.model.Income;
import com.practica.empresarial.veterinaria.model.dto.IncomeDto;
import com.practica.empresarial.veterinaria.model.transform.IncomeTransform;
import com.practica.empresarial.veterinaria.repository.IncomeRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
class IncomeServicesTest {

    @Autowired
    private MessageUtil messageUtil;
    private IncomeServices incomeServices;
    private IncomeRepository incomeRepository;
    private Income sampleIncome;
    private IncomeDto sampleIncomeDto;
    private IncomeDto sampleIncomeDtoCreate;
    private List<Income> sampleDataTest;
    private List<Income> sampleDataTestEmpty;

    @BeforeEach
    public void setup() {
        incomeRepository = mock(IncomeRepository.class);
        IncomeTransform incomeTransform = new IncomeTransform();

        incomeServices = new IncomeServices(incomeRepository, incomeTransform);

        sampleIncome = new IncomeFactory().newInstance();

        sampleIncomeDto = new IncomeFactory().newInstanceDto();

        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(sampleIncome);
    }

    @Test
    void getAll() {
        when(incomeRepository.findAll()).thenReturn(sampleDataTest);
        List<IncomeDto> dataConsulted = incomeServices.getAllIncome();
        assertThat(dataConsulted).isNotEmpty();
    }

}
