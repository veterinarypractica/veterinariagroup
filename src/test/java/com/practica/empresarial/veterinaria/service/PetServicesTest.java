package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.PetException;
import com.practica.empresarial.veterinaria.factorie.PetFactory;
import com.practica.empresarial.veterinaria.model.Pet;
import com.practica.empresarial.veterinaria.model.dto.PetDto;
import com.practica.empresarial.veterinaria.model.transform.PetTransform;
import com.practica.empresarial.veterinaria.repository.PetRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class PetServicesTest {

    @Autowired
    private MessageUtil messageUtil;
    private PetServices petServices;
    private PetRepository petRepository;

    private List<Pet> sampleDataTest;
    private List<Pet> sampleDataTestEmpty;


    @BeforeEach
    public void setup() {
        petRepository = mock(PetRepository.class);
        PetTransform petTransform = new PetTransform();
        petServices = new PetServices(petRepository, messageUtil, petTransform);
        Pet samplePet = new PetFactory().newInstance();
        sampleDataTest = new ArrayList<>();
        sampleDataTestEmpty = new ArrayList<>();
        sampleDataTest.add(samplePet);
    }

    @Test
    void getExistingUserToValidateObject() {
        when(petRepository.findAll()).thenReturn(sampleDataTest);
        String idUser = "1";
        List<PetDto> dataConsulted = petServices.getAllPetByUser(idUser);
        dataConsulted.forEach(pet -> assertThat(pet.getIdUser()).isPositive());
    }

    @Test
    void getExistingUser() {
        when(petRepository.findAllByDocumentUser(Mockito.anyString())).thenReturn(sampleDataTest);
        String idUser = "1";
        List<PetDto> dataConsulted = petServices.getAllPetByUser(idUser);
        assertThat(dataConsulted).isNotEmpty();
    }

    @Test
    void getNoExistingUser() {
        String idUser = "2222222222";
        when(petRepository.findAllByDocumentUser(Mockito.anyString())).thenReturn(sampleDataTestEmpty);
        List<PetDto> dataConsulted = petServices.getAllPetByUser(idUser);
        assertThat(dataConsulted).isEmpty();

    }

    @Test
    void testIdUserNull() {
        when(petRepository.findAllByDocumentUser(null)).thenThrow(IllegalArgumentException.class);
        Exception exception = assertThrows(PetException.class, () -> petServices.getAllPetByUser(null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


}

