package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.MedicalExamException;
import com.practica.empresarial.veterinaria.factorie.ExamDetailFactory;
import com.practica.empresarial.veterinaria.factorie.MedicalExamFactory;
import com.practica.empresarial.veterinaria.model.ExamDetail;
import com.practica.empresarial.veterinaria.model.MedicalExam;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import com.practica.empresarial.veterinaria.model.dto.MedicalExamDto;
import com.practica.empresarial.veterinaria.model.transform.MedicalExamTransform;
import com.practica.empresarial.veterinaria.repository.MedicalExamRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import com.practica.empresarial.veterinaria.utils.PdfUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class MedicalExamServicesTest {

    PdfUtil pdfUtil;
    @Autowired
    private MessageUtil messageUtil;
    private MedicalExamServices medicalExamServices;
    private MedicalExamRepository medicalExamRepository;

    private MedicalExam sampleMedicalExam;
    private ExamDetail sampleExamDetail;
    private ExamDetailDto sampleExamDetailDto;
    private ExamDetailService examDetailService;
    private MailUtil mailUtil;
    private List<MedicalExam> sampleDataTest;
    private List<ExamDetail> sampleDataDetailTest;
    private List<ExamDetailDto> sampleDataDetailDtoTest;

    @BeforeEach
    void setup() {
        medicalExamRepository = mock(MedicalExamRepository.class);
        examDetailService = mock(ExamDetailService.class);
        mailUtil = mock(MailUtil.class);
        pdfUtil = mock(PdfUtil.class);
        MedicalExamTransform medicalExamTransform = new MedicalExamTransform();

        medicalExamServices = new MedicalExamServices
                (
                        medicalExamRepository,
                        medicalExamTransform,
                        messageUtil,
                        examDetailService,
                        mailUtil,
                        pdfUtil
                );
        sampleMedicalExam = new MedicalExamFactory().newInstance();

        sampleExamDetail = new ExamDetailFactory().newInstance();
        sampleExamDetailDto = new ExamDetailFactory().newInstanceDto();

        sampleDataTest = new ArrayList<>();

        sampleDataDetailTest = new ArrayList<>();
        sampleDataDetailDtoTest = new ArrayList<>();
        sampleDataDetailDtoTest.add(sampleExamDetailDto);

        sampleDataDetailTest.add(sampleExamDetail);
        sampleDataTest.add(sampleMedicalExam);
    }


    @Test
    void testGetAllMedicalExamByPet() {
        when(medicalExamRepository.getAllByPet(Mockito.anyLong())).thenReturn(sampleDataTest);
        List<MedicalExamDto> actualAnswer = medicalExamServices.getAllMedicalExamByPet(1L);
        assertThat(actualAnswer).isNotEmpty();
    }


    @Test
    void testGetAllMedicalExamByPetFailed() {
        when(medicalExamRepository.getAllByPet(Mockito.anyLong())).thenReturn(sampleDataTest);
        Exception exception = assertThrows(MedicalExamException.class, () -> medicalExamServices.getAllMedicalExamByPet(null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testSendEmailWithPDForParamsNull() {
        Exception exception = assertThrows(MedicalExamException.class, () -> medicalExamServices.sendByEmailMedicalExamReport(null, null));
        String expectedMessage = "el campo está vacio";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

//    @Test
//    void testSendEmailWithPDForParams() {
//        when(medicalExamRepository.getOneByIdMedicalExam(Mockito.anyLong())).thenReturn(sampleMedicalExam);
//        when(examDetailService.getAllExamDetailByIdExam(Mockito.anyLong())).thenReturn(sampleDataDetailDtoTest);
//        doNothing().when(mailUtil).sendEmailWithAttachment(Mockito.any(MailBodyDto.class), Mockito.anyString(), Mockito.any(FileSystemResource.class));
//        medicalExamServices.sendByEmailMedicalExamReport("sebas199765@gmail.com", 1L);
//    }
}
