package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import com.practica.empresarial.veterinaria.model.Veterinary;
import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.VeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.VeterinaryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class VeterinaryServicesTest {

    @Autowired
    private MessageUtil messageUtil;

    private VeterinaryServices veterinaryServices;
    private VeterinaryRepository veterinaryRepository;
    private Veterinary sampleVeterinary;
    private List<Veterinary> dataTest;
    private List<Veterinary> dataTestEmpty;

    @BeforeEach
    public void setup() {
        veterinaryRepository = mock(VeterinaryRepository.class);
        VeterinaryTransform veterinaryTransform = new VeterinaryTransform();
        veterinaryServices = new VeterinaryServices(veterinaryRepository, veterinaryTransform, messageUtil);
        sampleVeterinary = Veterinary.builder()
                .idVeterinary(25L)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();
        dataTest = new ArrayList<>();
        dataTestEmpty = new ArrayList<>();
        dataTest.add(sampleVeterinary);

    }

    @Test
    void insertVeterinaryValidFields() {
        when(veterinaryRepository.save(Mockito.any(Veterinary.class))).thenReturn(sampleVeterinary);
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();
        VeterinaryDto actualAnswer = veterinaryServices.createVeterinary(veterinary);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void insertVeterinaryIdFieldNotNull() {
        when(veterinaryRepository.save(Mockito.any(Veterinary.class))).thenReturn(null);
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(3L)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(5000115L)
                .owner(5L)
                .status(true)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertVeterinaryNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name(null)
                .email(null)
                .nit(null)
                .owner(null)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertVeterinaryStatusNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("veterinar")
                .email("se@gmail.com")
                .nit(123L)
                .owner(123L)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertVeterinaryEmailNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("veterinar")
                .email(null)
                .nit(null)
                .owner(null)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertVeterinaryNitNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("veterinar")
                .email("se@gmail.com")
                .nit(null)
                .owner(null)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertVeterinaryOwnerNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("veterinar")
                .email("se@gmail.com")
                .nit(123L)
                .owner(null)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void insertNullVeterinary() {
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(null));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualAnswer).isEqualTo(expectedMessage);
    }

    @Test
    void insertVeterinaryValueFieldZero() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets3")
                .email("srincon@gmail.com")
                .nit(0L)
                .owner((long) 5)
                .status(true)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).isEqualTo(expectedMessage);
    }

    @Test
    void insertVeterinaryValueOwnerFieldZero() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets3")
                .email("srincon@gmail.com")
                .nit(2L)
                .owner(0L)
                .status(true)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.createVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).isEqualTo(expectedMessage);
    }

    @Test
    void updateVeterinaryValidFields() {
        when(veterinaryRepository.save(Mockito.any(Veterinary.class))).thenReturn(sampleVeterinary);
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(1L)
                .name("zoopets2")
                .email("zoopets@gmail.com")
                .nit(1144201661L)
                .owner(5L)
                .status(true)
                .build();
        VeterinaryDto actualAnswer = veterinaryServices.updateVeterinary(veterinary);
        assertThat(actualAnswer).isNotNull();
    }

    @Test
    void updateVeterinaryIdFieldNull() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(null)
                .name("zoopets")
                .email("zoopets@gmail.com")
                .nit(5000115L)
                .owner(5L)
                .status(true)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.updateVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void updateVeterinaryNullFields() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(1L)
                .name(null)
                .email(null)
                .nit(null)
                .owner(null)
                .status(null)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.updateVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "los campos están vacios";
        assertThat(actualAnswer).contains(expectedMessage);
    }

    @Test
    void updateNullVeterinary() {
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.updateVeterinary(null));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "Error al construir.";
        assertThat(actualAnswer).isEqualTo(expectedMessage);
    }

    @Test
    void updateVeterinaryValueFieldZero() {
        VeterinaryDto veterinary = VeterinaryDto.builder()
                .idVeterinary(1L)
                .name("zoopets3")
                .email("srincon@gmail.com")
                .nit(0L)
                .owner((long) 5)
                .status(true)
                .build();
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.updateVeterinary(veterinary));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el valor ingresado no esta permitido";
        assertThat(actualAnswer).isEqualTo(expectedMessage);
    }

    @Test
    void getAllVeterinary() {
        when(veterinaryRepository.findAll()).thenReturn(dataTest);
        List<VeterinaryDto> veterinaries = veterinaryServices.getAllVeterinary();
        int actualSize = veterinaries.size();
        assertTrue(actualSize > 0);
    }

    @Test
    void getAllVeterinaryByNameExist() {
        when(veterinaryRepository.findAllByName(sampleVeterinary.getName())).thenReturn(dataTest);
        String name = "zoopets";
        List<VeterinaryDto> veterinaries = veterinaryServices.getAllVeterinaryByName(name);
        int actualSize = veterinaries.size();
        assertThat(actualSize).isPositive();
    }

    @Test
    void getAllVeterinaryByNameNotExist() {
        when(veterinaryRepository.findAllByName("goodYear")).thenReturn(dataTestEmpty);
        String name = "goodYear";
        List<VeterinaryDto> veterinaries = veterinaryServices.getAllVeterinaryByName(name);
        int actualSize = veterinaries.size();
        assertEquals(0, actualSize);
    }

    @Test
    void getAllVeterinaryByNameNull() {
        Exception exception = assertThrows(VeterinaryException.class, () -> veterinaryServices.getAllVeterinaryByName(null));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "el campo está vacio";
        assertTrue(actualAnswer.contains(expectedMessage));
    }

    @Test
    void deleteVeterinaryNullId() {
        Exception exception = assertThrows(VeterinaryException.class,
                () -> veterinaryServices.deleteVeterinary(null));
        String actualAnswer = exception.getMessage();
        String expectedMessage = "campo id erroneo";
        assertTrue(actualAnswer.contains(expectedMessage));
    }

    @Test
    void deleteVeterinaryId() {
        when(veterinaryRepository.findById(1L)).thenReturn(Optional.of(sampleVeterinary));
        String actualMessage = veterinaryServices.deleteVeterinary(1L);
        verify(veterinaryRepository, times(1)).deleteById(1L);
        String expectedMessage = "Se ha eliminado exisitosamente";
        assertTrue(actualMessage.contains(expectedMessage));

    }

}