package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.ClinicalHistoryDetail;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;

import java.util.Date;

public class ClinicalHistoryDetailFactorie {

    private Long idDetailHC;
    private Float temperature;
    private Float weight;
    private Float heartRate;
    private Float breathingFrequency;
    private Date admissionDate;
    private Long idClinicalHistory;
    private String feeding;
    private Date dewormingDate;
    private String habitat;
    private Float capillaryTime;
    private String reproductiveStatus;
    private String observations;
    private Long idCollaborator;

    public ClinicalHistoryDetailFactorie() {
        this.idDetailHC = 1L;
        this.temperature = 33f;
        this.weight = 40f;
        this.heartRate = 22.5f;
        this.breathingFrequency = 20.4f;
        this.admissionDate = new Date();
        this.idClinicalHistory = 2L;
        this.feeding = "croquetas";
        this.dewormingDate = new Date();
        this.habitat = "hogar";
        this.capillaryTime = 30f;
        this.reproductiveStatus = "castrado";
        this.observations = "mascota en buen estado de salud";
        this.idCollaborator = 3L;
    }

    public ClinicalHistoryDetail newInstance() {
        return ClinicalHistoryDetail.builder()
                .idDetailHC(this.idDetailHC)
                .temperature(this.temperature)
                .weight(this.weight)
                .heartRate(this.heartRate)
                .breathingFrequency(this.breathingFrequency)
                .admissionDate(this.admissionDate)
                .idClinicalHistory(this.idClinicalHistory)
                .feeding(this.feeding)
                .dewormingDate(this.dewormingDate)
                .habitat(this.habitat)
                .capillaryTime(this.capillaryTime)
                .reproductiveStatus(this.reproductiveStatus)
                .observations(this.observations)
                .idCollaborator(this.idCollaborator)
                .build();
    }


    public ClinicalHistoryDetailDto newInstanceDto() {
        return ClinicalHistoryDetailDto.builder()
                .idDetailHC(this.idDetailHC)
                .temperature(this.temperature)
                .weight(this.weight)
                .heartRate(this.heartRate)
                .breathingFrequency(this.breathingFrequency)
                .admissionDate(this.admissionDate)
                .idClinicalHistory(this.idClinicalHistory)
                .feeding(this.feeding)
                .dewormingDate(this.dewormingDate)
                .habitat(this.habitat)
                .capillaryTime(this.capillaryTime)
                .reproductiveStatus(this.reproductiveStatus)
                .observations(this.observations)
                .idCollaborator(this.idCollaborator)
                .build();
    }

    public ClinicalHistoryDetailFactorie setIdDetailHc(Long idDetailHC) {
        this.idDetailHC = idDetailHC;
        return this;
    }

    public ClinicalHistoryDetailFactorie setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }

    public ClinicalHistoryDetailFactorie setWeight(Float weight) {
        this.weight = weight;
        return this;
    }

    public ClinicalHistoryDetailFactorie setHeartRate(Float heartRate) {
        this.heartRate = heartRate;
        return this;
    }

    public ClinicalHistoryDetailFactorie setBreathingFrequency(Float breathingFrequency) {
        this.breathingFrequency = breathingFrequency;
        return this;
    }

    public ClinicalHistoryDetailFactorie setAdmissionDate(Date admissionDate) {
        this.admissionDate = admissionDate;
        return this;
    }


    public ClinicalHistoryDetailFactorie setIdClinicalHistory(Long idClinicalHistory) {
        this.idClinicalHistory = idClinicalHistory;
        return this;
    }

    public ClinicalHistoryDetailFactorie setFeeding(String feeding) {
        this.feeding = feeding;
        return this;
    }

    public ClinicalHistoryDetailFactorie setDewormingDate(Date dewormingDate) {
        this.dewormingDate = dewormingDate;
        return this;
    }

    public ClinicalHistoryDetailFactorie setHabitat(String habitat) {
        this.habitat = habitat;
        return this;
    }

    public ClinicalHistoryDetailFactorie setCapillaryTime(Float capillaryTime) {
        this.capillaryTime = capillaryTime;
        return this;
    }

    public ClinicalHistoryDetailFactorie setReproductiveStatus(String reproductiveStatus) {
        this.reproductiveStatus = reproductiveStatus;
        return this;
    }

    public ClinicalHistoryDetailFactorie setObservations(String observations) {
        this.observations = observations;
        return this;
    }

    public ClinicalHistoryDetailFactorie setIdCollaborator(Long idCollaborator) {
        this.idCollaborator = idCollaborator;
        return this;
    }

}
