package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.ClinicalHistory;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;

import java.util.Date;

public class ClinicalHistoryFactorie {

    private Long idClinicalHistory;
    private Long idPet;
    private Date dateCreate;

    public ClinicalHistoryFactorie() {
        this.idClinicalHistory = 1L;
        this.idPet = 2L;
        this.dateCreate = new Date();

    }

    public ClinicalHistory newInstance() {
        return ClinicalHistory.builder()
                .idClinicalHistory(this.idClinicalHistory)
                .idPet(this.idPet)
                .dateCreate(this.dateCreate)
                .build();
    }

    public ClinicalHistoryDto newInstanceDto() {
        return ClinicalHistoryDto.builder()
                .idClinicalHistory(this.idClinicalHistory)
                .idPet(this.idPet)
                .dateCreate(this.dateCreate)
                .build();
    }

    public ClinicalHistoryFactorie setIdClinicalHistory(Long idClinicalHistory) {

        this.idClinicalHistory = idClinicalHistory;
        return this;
    }

    public ClinicalHistoryFactorie setIdPet(Long idPet) {

        this.idPet = idPet;
        return this;
    }

    public ClinicalHistoryFactorie setDateCreate(Date dateCreate) {

        this.dateCreate = dateCreate;
        return this;
    }

}
