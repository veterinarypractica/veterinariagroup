package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;

public class ServiceVeterinaryFactory {

    private Long idService;
    private String nameService;
    private Long quantityService;
    private Float valueService;
    private String descriptionService;
    private Boolean statusService;

    public ServiceVeterinaryFactory() {
        this.idService = 1L;
        this.nameService = "peluqueria";
        this.quantityService = 1L;
        this.valueService = 15000F;
        this.descriptionService = "Corte de pelo";
        this.statusService = true;
    }

    public ServiceVeterinary newInstance() {
        return ServiceVeterinary.builder()
                .idService(this.idService)
                .nameService(this.nameService)
                .quantityService(this.quantityService)
                .valueService(this.valueService)
                .descriptionService(this.descriptionService)
                .statusService(this.statusService)
                .build();
    }

    public ServiceVeterinaryDto newInstanceDto() {
        return ServiceVeterinaryDto.builder()
                .idService(this.idService)
                .nameService(this.nameService)
                .quantityService(this.quantityService)
                .valueService(this.valueService)
                .descriptionService(this.descriptionService)
                .statusService(this.statusService)
                .build();
    }

    public ServiceVeterinaryFactory setIdService(Long id) {
        this.idService = id;
        return this;
    }

    public ServiceVeterinaryFactory setNameService(String name) {
        this.nameService = name;
        return this;
    }

    public ServiceVeterinaryFactory setQuantityService(Long quantity) {
        this.quantityService = quantity;
        return this;
    }

    public ServiceVeterinaryFactory setValueService(Float value) {
        this.valueService = value;
        return this;
    }

    public ServiceVeterinaryFactory setDescriptionService(String description) {
        this.descriptionService = description;
        return this;
    }

    public ServiceVeterinaryFactory setStatusService(Boolean status) {
        this.statusService = status;
        return this;
    }
}
