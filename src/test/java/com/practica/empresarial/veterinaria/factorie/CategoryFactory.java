package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Category;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;

public class CategoryFactory {

    private Long idCategory;
    private String nameCategory;
    private Boolean statusCategory;
    private String descriptionCategory;

    public CategoryFactory() {
        this.idCategory = 1L;
        this.nameCategory = "estetica";
        this.statusCategory = true;
        this.descriptionCategory = "encargada de la belleza de las mascotas";
    }

    public Category newInstance() {
        return Category.builder()
                .idCategory(this.idCategory)
                .description(this.descriptionCategory)
                .status(this.statusCategory)
                .name(this.nameCategory)
                .build();
    }

    public CategoryDto newInstanceDto() {
        return CategoryDto.builder()
                .idCategory(this.idCategory)
                .descriptionCategory(this.descriptionCategory)
                .statusCategory(this.statusCategory)
                .nameCategory(this.nameCategory)
                .build();
    }

    public CategoryFactory setIdCategory(Long idCategory) {

        this.idCategory = idCategory;
        return this;
    }

    public CategoryFactory setNameCategory(String nameCategory) {

        this.nameCategory = nameCategory;
        return this;
    }

    public CategoryFactory setStatusCategory(Boolean statusCategory) {

        this.statusCategory = statusCategory;
        return this;
    }

    public CategoryFactory setDescriptionCategory(String descriptionCategory) {

        this.descriptionCategory = descriptionCategory;
        return this;
    }
}
