package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.BranchOffice;
import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;

public class


BranchOfficeFactory {

    private Long idBranchOffice;
    private Long branchPhone;
    private String address;
    private String logo;
    private Long admin;
    private Long idVeterinary;


    public BranchOfficeFactory() {
        this.idBranchOffice = 1L;
        this.branchPhone = 3123196836L;
        this.address = "street11";
        this.logo = "logo.png";
        this.admin = 1144201661L;
        this.idVeterinary = 9L;

    }

    public BranchOffice newInstance() {
        return BranchOffice.builder()
                .idBranchOffice(this.idBranchOffice)
                .address(this.address)
                .branchPhone(this.branchPhone)
                .logo(this.logo)
                .admin(this.admin)
                .idVeterinaria(this.idVeterinary)
                .build();
    }

    public BranchOfficeDto newInstanceDto() {
        return BranchOfficeDto.builder()
                .idBranchOffice(this.idBranchOffice)
                .address(this.address)
                .branchPhone(this.branchPhone)
                .logo(this.logo)
                .admin(this.admin)
                .idVeterinary(this.idVeterinary)
                .build();
    }

    public BranchOfficeFactory setId(Long id) {
        this.idBranchOffice = id;
        return this;
    }

    public BranchOfficeFactory setBranchPhone(Long branchPhone) {
        this.branchPhone = branchPhone;
        return this;
    }

    public BranchOfficeFactory setAddress(String address) {
        this.address = address;
        return this;
    }

    public BranchOfficeFactory setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    public BranchOfficeFactory setAdmin(Long admin) {
        this.admin = admin;
        return this;
    }

    public BranchOfficeFactory setIdVeterinary(Long idVeterinary) {
        this.idVeterinary = idVeterinary;
        return this;
    }


}
