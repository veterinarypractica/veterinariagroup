package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.Veterinary;
import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;

public class VeterinaryFactory {

    private Long idVeterinary;
    private String name;
    private Long nit;
    private String email;
    private Long owner;
    private Boolean status;


    public VeterinaryFactory() {
        this.idVeterinary = 25L;
        this.name = "zoopets";
        this.nit = 1144201661L;
        this.email = "zoopets@gmail.com";
        this.owner = 5L;
        this.status = true;

    }

    public Veterinary newInstance() {
        return Veterinary.builder()
                .idVeterinary(this.idVeterinary)
                .name(this.name)
                .email(this.email)
                .nit(this.nit)
                .owner(this.owner)
                .status(this.status)
                .build();
    }

    public VeterinaryDto newInstanceDto() {
        return VeterinaryDto.builder()
                .idVeterinary(this.idVeterinary)
                .name(this.name)
                .email(this.email)
                .nit(this.nit)
                .owner(this.owner)
                .status(this.status)
                .build();

    }

    public VeterinaryFactory setId(Long id) {
        this.idVeterinary = id;
        return this;
    }

    public VeterinaryFactory setName(String name) {
        this.name = name;
        return this;
    }

    public VeterinaryFactory setEmail(String email) {
        this.email = email;
        return this;
    }

    public VeterinaryFactory setNit(Long nit) {
        this.nit = nit;
        return this;
    }

    public VeterinaryFactory setOwner(Long owner) {
        this.nit = owner;
        return this;
    }

    public VeterinaryFactory setStatus(Boolean status) {
        this.status = status;
        return this;
    }

}


