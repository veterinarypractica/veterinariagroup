package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.DetailInvoice;
import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;

public class DetailInvoiceFactory {

    private Long idDetail;
    private Long quantity;
    private Long price;
    private Integer idService;
    private Integer idInvoice;
    ;
    private Integer idColaborator;


    public DetailInvoiceFactory() {
        this.idDetail = 1L;
        this.quantity = 1L;
        this.price = 1L;
        this.idService = 3;
        this.idInvoice = 2;
        this.idColaborator = 2;
    }

    public DetailInvoice newInstance() {
        return DetailInvoice.builder()
                .idDetail(this.idDetail)
                .quantity(this.quantity)
                .price(this.price)
                .idService(this.idService)
                .idInvoice(this.idInvoice)
                .idColaborator(this.idColaborator)
                .build();
    }

    public DetailInvoiceDto newInstanceDto() {
        return DetailInvoiceDto.builder()
                .idDetail(this.idDetail)
                .quantity(this.quantity)
                .price(this.price)
                .idService(this.idService)
                .idInvoice(this.idInvoice)
                .idColaborator(this.idColaborator)
                .build();

    }

    public DetailInvoiceFactory setId(Long id) {
        this.idDetail = id;
        return this;
    }

    public DetailInvoiceFactory setQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public DetailInvoiceFactory setPrice(Long price) {
        this.price = price;
        return this;
    }

    public DetailInvoiceFactory setIdService(Integer idService) {
        this.idService = idService;
        return this;
    }

    public DetailInvoiceFactory setIdInvoice(Integer idInvoice) {
        this.idInvoice = idInvoice;
        return this;
    }


}
