package com.practica.empresarial.veterinaria.factorie;

import com.practica.empresarial.veterinaria.model.UserVeterinary;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;

import java.util.Date;

public class UserVeterinaryFactory {

    private Long idUser;
    private String name;
    private String lastName;
    private String documentType;
    private String documentNumber;
    private Date birthDate;
    private String department;
    private String city;
    private String neighborhood;
    private Long phone;
    private String email;
    private String userName;
    private String pass;
    private Boolean status;
    private Integer rol;
    private String token;

    public UserVeterinaryFactory() {
        this.idUser = 1L;
        this.name = "Ingrith";
        this.lastName = "Santamaria";
        this.documentType = "cc";
        this.documentNumber = "1101760015";
        this.birthDate = new Date();
        this.department = "Valle del Cauca";
        this.city = "Cali";
        this.neighborhood = "Alamos";
        this.phone = 3215880165L;
        this.email = "ingrithsanta@gmail.com";
        this.userName = "south97";
        this.pass = "8194";
        this.status = true;
        this.rol = 1;
    }

    public UserVeterinary newInstance() {
        return UserVeterinary.builder()
                .idUser(this.idUser)
                .name(this.name)
                .lastName(this.lastName)
                .documentType(this.documentType)
                .documentNumber(this.documentNumber)
                .birthDate(this.birthDate)
                .department(this.department)
                .city(this.city)
                .neighborhood(this.neighborhood)
                .phone(this.phone)
                .email(this.email)
                .userName(this.userName)
                .pass(this.pass)
                .status(this.status)
                .rol(this.rol)
                .build();
    }

    public UserVeterinaryDto newInstanceDto() {
        return UserVeterinaryDto.builder()
                .idUser(this.idUser)
                .name(this.name)
                .lastName(this.lastName)
                .documentType(this.documentType)
                .documentNumber(this.documentNumber)
                .birthDate(this.birthDate)
                .department(this.department)
                .city(this.city)
                .neighborhood(this.neighborhood)
                .phone(this.phone)
                .email(this.email)
                .userName(this.userName)
                .pass(this.pass)
                .status(this.status)
                .rol(this.rol)
                .build();
    }

    public UserVeterinaryFactory setIdUser(Long idUser) {
        this.idUser = idUser;
        return this;
    }

    public UserVeterinaryFactory setName(String name) {
        this.name = name;
        return this;
    }

    public UserVeterinaryFactory setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserVeterinaryFactory setDocumentType(String documentType) {
        this.documentType = documentType;
        return this;
    }

    public UserVeterinaryFactory setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
        return this;
    }

    public UserVeterinaryFactory setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public UserVeterinaryFactory setDepartment(String department) {
        this.department = department;
        return this;
    }

    public UserVeterinaryFactory setCity(String city) {
        this.city = city;
        return this;
    }

    public UserVeterinaryFactory setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
        return this;
    }

    public UserVeterinaryFactory setPhone(Long phone) {
        this.phone = phone;
        return this;
    }

    public UserVeterinaryFactory setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserVeterinaryFactory setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public UserVeterinaryFactory setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public UserVeterinaryFactory setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public UserVeterinaryFactory setRol(Integer rol) {
        this.rol = rol;
        return this;
    }
}
