package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;
import com.practica.empresarial.veterinaria.model.transform.ServiceVeterinaryTransform;
import com.practica.empresarial.veterinaria.repository.ServiceVeterinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class ServiceVeterinaryServices {

    private final ServiceVeterinaryTransform serviceVeterinaryTransform;
    private final ServiceVeterinaryRepository serviceVeterinaryRepository;

    @Autowired
    public ServiceVeterinaryServices(ServiceVeterinaryRepository serviceVeterinaryRepository, ServiceVeterinaryTransform serviceVeterinaryTransform) {
        this.serviceVeterinaryRepository = serviceVeterinaryRepository;
        this.serviceVeterinaryTransform = serviceVeterinaryTransform;
    }

    public ServiceVeterinaryDto createServices(ServiceVeterinaryDto service) {


        ServiceVeterinary serviceDb = serviceVeterinaryTransform.serviceVeterinaryDtoToServiceVeterinary(service);
        serviceDb.validateCreate();
        return serviceVeterinaryTransform.serviceVeterinaryToServiceVeteinaryDto(serviceVeterinaryRepository.save(serviceDb));

    }

    public ServiceVeterinaryDto updateService(ServiceVeterinaryDto service) {

        ServiceVeterinary serviceVeterinaryDB = serviceVeterinaryTransform.serviceVeterinaryDtoToServiceVeterinary(service);
        serviceVeterinaryDB.validateUpdate();
        serviceVeterinaryRepository.save(serviceVeterinaryDB);
        return serviceVeterinaryTransform.serviceVeterinaryToServiceVeteinaryDto(serviceVeterinaryRepository.save(serviceVeterinaryDB));

    }

    public List<ServiceVeterinaryDto> getAllServiceByNameService(String nameService) {

        return serviceVeterinaryRepository.findAllByNameService(nameService)
                .stream()
                .map(serviceVeterinaryTransform::serviceVeterinaryToServiceVeteinaryDto)
                .collect(Collectors.toList());
    }

    public List<ServiceVeterinaryDto> getAllServiceByType(Integer idCategory) {

        return serviceVeterinaryRepository.findAllByIdCategory(idCategory)
                .stream()
                .map(serviceVeterinaryTransform::serviceVeterinaryToServiceVeteinaryDto)
                .collect(Collectors.toList());
    }
}
