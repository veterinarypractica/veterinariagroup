package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.CategoryException;
import com.practica.empresarial.veterinaria.model.Category;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;
import com.practica.empresarial.veterinaria.model.transform.CategoryTransform;
import com.practica.empresarial.veterinaria.repository.CategoryRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CategoryService {

    private final CategoryTransform categoryTransform;
    private final CategoryRepository categoryRepository;
    private final MessageUtil messageUtil;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository, CategoryTransform categoryDtoToCategory, MessageUtil messageResolve) {
        this.categoryRepository = categoryRepository;
        this.categoryTransform = categoryDtoToCategory;
        this.messageUtil = messageResolve;
    }

    public CategoryDto createCategory(CategoryDto category) {
        try {
            Category categoryPersist = categoryTransform.categoryDtoToCategory(category);
            categoryPersist.validateCategoryCreate();
            return categoryTransform.categoryToCategoryDto(categoryRepository.save(categoryPersist));
        } catch (NullPointerException e) {
            throw new CategoryException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public CategoryDto updateCategory(CategoryDto category) {
        try {
            Category categoryPersist = categoryTransform.categoryDtoToCategory(category);
            categoryPersist.validateCategoryUpdate();
            return categoryTransform.categoryToCategoryDto(categoryRepository.save(categoryPersist));
        } catch (NullPointerException e) {
            throw new CategoryException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public CategoryDto getOneByName(String name) {

        if (StringUtils.isNotBlank(name)) {
            return categoryTransform.categoryToCategoryDto(categoryRepository.getOneByName(name).orElse(null));
        }
        throw new CategoryException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }
}
