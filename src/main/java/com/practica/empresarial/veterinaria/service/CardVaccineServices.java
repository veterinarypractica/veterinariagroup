package com.practica.empresarial.veterinaria.service;

import com.itextpdf.text.pdf.PdfPTable;
import com.practica.empresarial.veterinaria.exceptions.CardVaccineException;
import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;
import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import com.practica.empresarial.veterinaria.model.transform.CardVaccineTransform;
import com.practica.empresarial.veterinaria.repository.CardVaccineRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import com.practica.empresarial.veterinaria.utils.PdfUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CardVaccineServices {

    private final DetailVaccineServices detailVaccineServices;
    private final CardVaccineRepository cardVaccineRepository;
    private final CardVaccineTransform cardVaccineTransform;
    private final MessageUtil messageUtil;
    private final MailUtil mailUtil;
    private final PdfUtil pdfUtil;


    @Autowired
    public CardVaccineServices(CardVaccineRepository cardVaccineRepository, CardVaccineTransform cardVaccineTransform, MessageUtil messageResolve, DetailVaccineServices detailVaccineServices, MailUtil mailUtil, PdfUtil pdfUtil) {
        this.cardVaccineRepository = cardVaccineRepository;
        this.cardVaccineTransform = cardVaccineTransform;
        this.messageUtil = messageResolve;
        this.detailVaccineServices = detailVaccineServices;
        this.mailUtil = mailUtil;
        this.pdfUtil = pdfUtil;
    }

    public CardVaccineDto getOneByIdPet(Integer idPet) {
        return cardVaccineTransform.cardVaccineToCardVaccineDto(cardVaccineRepository.findByIdPet(idPet).orElse(null));
    }

    public Map<String, Object> getCardVaccine(Integer idPet) {
        Map<String, Object> data = new HashMap<>();
        CardVaccineDto cardVaccine = getOneByIdPet(idPet);
        List<DetailVaccineDto> cardDetVaccine = detailVaccineServices.getAllByIdCardVaccine(cardVaccine.getIdCardVaccine().intValue());
        data.put("maestro", cardVaccine);
        data.put("detalle", cardDetVaccine);
        return data;
    }

    private List<DetailVaccineDto> getDetailCardVaccine(Integer idPet) {
        return detailVaccineServices.getAllByIdCardVaccine(idPet);
    }

    public String sendByEmailCardVaccine(String userEmail, Integer idPet) {
        if (StringUtils.isNotBlank(userEmail) && null != idPet) {

            String title = getNamePdf(idPet);

            MailBodyDto emailBody = MailBodyDto.builder()
                    .email(userEmail)
                    .content("Carnet de vacunas se adjunta archivo PDF")
                    .subject("carnet de vacunas")
                    .build();

            FileSystemResource file = new FileSystemResource(title);
            mailUtil.sendEmailWithAttachment(emailBody, title, file);
            return messageUtil.resolveMessageSource("successful.send.mail");
        }
        throw new CardVaccineException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    private String getNamePdf(Integer idPet) throws VeterinaryException {
        CardVaccineDto cardVac = getOneByIdPet(idPet);
        return pdfUtil.generatePdf(
                "carnet_de_vacunas.pdf",
                generateTableHeader(cardVac),
                generateTableContent(idPet),
                null);
    }

    private PdfPTable generateTableHeader(CardVaccineDto cardVac) {

        PdfPTable tableHeader = new PdfPTable(1);
        tableHeader.addCell("Mascota" + cardVac.getIdPet() + "\n" + "Codigo del carnet de vacunación:" + cardVac.getIdCardVaccine());
        return tableHeader;
    }

    private PdfPTable generateTableContent(Integer idPet) {

        PdfPTable tableContent = new PdfPTable(5);
        pdfUtil.addTableTitle(tableContent, new String[]{"Nombre Vacuna", "Dosis Vacuna", "Fecha Vacuna", "Codigo Vacuna", "Estado Vacuna"});
        addRows(tableContent, idPet);
        return tableContent;
    }

    private void addRows(PdfPTable table, Integer idPet) {
        if (null != table && null != idPet) {
            getDetailCardVaccine(idPet).forEach(cv -> {
                table.addCell(cv.getNameVaccine());
                table.addCell(Float.toString(cv.getDoseVaccine()));
                table.addCell(String.valueOf(cv.getDateVaccine()));
                table.addCell(Float.toString(cv.getCodeVaccine()));
                table.addCell(Boolean.toString(cv.getState()));
            });
        }
    }

}