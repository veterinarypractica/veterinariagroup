package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.BranchOfficeException;
import com.practica.empresarial.veterinaria.model.BranchOffice;
import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;
import com.practica.empresarial.veterinaria.model.transform.BranchTransform;
import com.practica.empresarial.veterinaria.repository.BranchOfficeRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class BranchOfficeServices {

    private final MessageUtil messageUtil;
    private final BranchOfficeRepository branchOfficeRepository;

    private final BranchTransform branchTransform;

    @Autowired
    public BranchOfficeServices(BranchOfficeRepository branchOfficeRepository,
                                BranchTransform branchTransform, MessageUtil messageUtil) {
        this.branchOfficeRepository = branchOfficeRepository;
        this.branchTransform = branchTransform;
        this.messageUtil = messageUtil;
    }

    public BranchOfficeDto createBranchOffice(BranchOfficeDto branchOffice) {

        try {
            BranchOffice branchOfficePersist = branchTransform.branchDtoToBranch(branchOffice);
            branchOfficePersist.validateCreate();
            return branchTransform.branchToBranchDto(branchOfficeRepository.save(branchOfficePersist));

        } catch (NullPointerException e) {
            throw new BranchOfficeException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public BranchOfficeDto updateBranchOffice(BranchOfficeDto branchOffice) {
        try {
            BranchOffice branchOfficePersist = branchTransform.branchDtoToBranch(branchOffice);
            branchOfficePersist.validateUpdate();

            return branchTransform.branchToBranchDto(branchOfficeRepository.save(branchOfficePersist));
        } catch (NullPointerException e) {
            throw new BranchOfficeException(messageUtil.resolveMessageSource("dto.null.exception.message"));
        }
    }

    public List<BranchOfficeDto> getAllBranchOffice() {
        return branchOfficeRepository.findAll()
                .stream()
                .map(branchTransform::branchToBranchDto)
                .collect(Collectors.toList());
    }

    public String deleteBranchOffice(Long idBranchOffice) {
        if (null != idBranchOffice) {
            branchOfficeRepository.deleteById(idBranchOffice);
            return messageUtil.resolveMessageSource("successful.delete.message");
        }
        throw new BranchOfficeException(messageUtil.resolveMessageSource("fieldEmpty.exception.message"));
    }
}
