package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.Egress;
import com.practica.empresarial.veterinaria.model.dto.EgressDto;
import com.practica.empresarial.veterinaria.model.transform.EgressTransform;
import com.practica.empresarial.veterinaria.repository.EgressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class EgressServices {

    private final EgressRepository egressRepository;
    private final EgressTransform egressTransform;

    @Autowired
    public EgressServices(EgressRepository egressRepository, EgressTransform egressTransform) {
        this.egressRepository = egressRepository;
        this.egressTransform = egressTransform;
    }

    public List<EgressDto> getAllEgress() {

        return egressRepository.findAll().stream()
                .map(egressTransform::egressToEgressDto)
                .collect(Collectors.toList());
    }

    public List<EgressDto> getAllEgressByType(String type) {

        return egressRepository.findAllByType(type).stream()
                .map(egressTransform::egressToEgressDto)
                .collect(Collectors.toList());
    }

    public List<EgressDto> getAllEgressByPet(Long idPet) {

        return egressRepository.findAllByIdPet(idPet).stream()
                .map(egressTransform::egressToEgressDto)
                .collect(Collectors.toList());
    }

    public EgressDto createEgress(EgressDto egress) {

        Egress egressDb = egressTransform.egressDtoToEgress(egress);
        egressDb.validateCreate();
        return egressTransform.egressToEgressDto(egressRepository.save(egressDb));
    }

    public EgressDto updateEgress(EgressDto egress) {

        Egress egressDb = egressTransform.egressDtoToEgress(egress);
        egressDb.validateUpdate();
        return egressTransform.egressToEgressDto(egressRepository.save(egressDb));
    }
}
