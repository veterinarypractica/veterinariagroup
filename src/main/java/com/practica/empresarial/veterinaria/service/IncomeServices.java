package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.dto.IncomeDto;
import com.practica.empresarial.veterinaria.model.transform.IncomeTransform;
import com.practica.empresarial.veterinaria.repository.IncomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class IncomeServices {
    private final IncomeRepository incomeRepository;
    private final IncomeTransform incomeTransform;

    @Autowired
    public IncomeServices(IncomeRepository incomeRepository, IncomeTransform incomeTransform) {
        this.incomeRepository = incomeRepository;
        this.incomeTransform = incomeTransform;
    }

    public List<IncomeDto> getAllIncome() {
        return incomeRepository.findAll().stream()
                .map(incomeTransform::incomeToIncomeDto)
                .collect(Collectors.toList());
    }

}
