package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.model.transform.DetailVaccineTransform;
import com.practica.empresarial.veterinaria.repository.DetailVaccineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DetailVaccineServices {

    private final DetailVaccineRepository detailVaccineRepository;
    private final DetailVaccineTransform detailVaccineTransform;

    @Autowired
    public DetailVaccineServices(DetailVaccineRepository detailVaccineRepository, DetailVaccineTransform detailVaccineTransform) {
        this.detailVaccineRepository = detailVaccineRepository;
        this.detailVaccineTransform = detailVaccineTransform;
    }

    public List<DetailVaccineDto> getAllByIdCardVaccine(Integer idCardVaccine) {
        return detailVaccineRepository.findAllByIdCardVaccine(idCardVaccine)
                .stream()
                .map(detailVaccineTransform::detailVaccineToDetailVaccineDto)
                .collect(Collectors.toList());
    }
}
