package com.practica.empresarial.veterinaria.service;

import com.practica.empresarial.veterinaria.exceptions.ExamDetailException;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;
import com.practica.empresarial.veterinaria.model.transform.ExamDetailTransform;
import com.practica.empresarial.veterinaria.repository.ExamDetailRepository;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExamDetailService {
    private final MessageUtil messageUtil;
    private final ExamDetailRepository examDetailRepository;
    private final ExamDetailTransform examDetailTransform;

    @Autowired
    public ExamDetailService(
            ExamDetailRepository examDetailRepository,
            ExamDetailTransform examDetailTransform,
            MessageUtil messageUtil) {
        this.examDetailRepository = examDetailRepository;
        this.examDetailTransform = examDetailTransform;
        this.messageUtil = messageUtil;
    }

    public List<ExamDetailDto> getAllExamDetailByIdExam(Long idExam) {
        if (null != idExam) {
            return examDetailRepository.findAllByIdExam(idExam)
                    .stream()
                    .map(examDetailTransform::examDetailToExamDetailDto)
                    .collect(Collectors.toList());
        }
        throw new ExamDetailException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }
}
