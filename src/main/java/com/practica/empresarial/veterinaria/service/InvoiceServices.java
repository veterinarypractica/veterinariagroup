package com.practica.empresarial.veterinaria.service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.practica.empresarial.veterinaria.exceptions.CardVaccineException;
import com.practica.empresarial.veterinaria.exceptions.InvoiceException;
import com.practica.empresarial.veterinaria.exceptions.MedicalExamException;
import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;
import com.practica.empresarial.veterinaria.model.dto.MailBodyDto;
import com.practica.empresarial.veterinaria.model.transform.InvoiceTransform;
import com.practica.empresarial.veterinaria.repository.InvoiceRepository;
import com.practica.empresarial.veterinaria.utils.MailUtil;
import com.practica.empresarial.veterinaria.utils.MessageUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class InvoiceServices {

    private final MessageUtil messageUtil;
    private final MailUtil mailUtil;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceTransform invoiceTransform;
    private final DetailInvoiceService detailInvoiceService;


    @Autowired
    public InvoiceServices(MessageUtil messageUtil, InvoiceRepository invoiceRepository, MailUtil mailUtil, InvoiceTransform invoiceTransform, DetailInvoiceService detailInvoiceService) {

        this.messageUtil = messageUtil;
        this.mailUtil = mailUtil;
        this.invoiceRepository = invoiceRepository;
        this.invoiceTransform = invoiceTransform;
        this.detailInvoiceService = detailInvoiceService;
    }


    /*public String sendEmailById(Long id) {
        MailBodyDto emailBody = MailBodyDto.builder()
                .email("sebas199765@gmail.com")
                .content("Contenido del correo")
                .subject("prueba correo" + id)
                .build();
        mailUtil.sendEmailWithoutAttachment(emailBody);
        return "Envio de correo exitoso";
    }*/

    public String sendEmailByIdIvnoice(String userEmail, Long idInvoice) {
        if (StringUtils.isNotBlank(userEmail) && null != idInvoice) {
            String title = getPDF(idInvoice);
            MailBodyDto emailBody = MailBodyDto.builder()
                    .email(userEmail)
                    .content("Se adjunta factura de servicio archivo PDF")
                    .subject("factura servicio")
                    .build();
            FileSystemResource file = new FileSystemResource(title);
            mailUtil.sendEmailWithAttachment(emailBody, title, file);
            return messageUtil.resolveMessageSource("successful.send.mail");
        }
        throw new CardVaccineException(messageUtil.resolveMessageSource("el campo está vacio"));

    }


    public List<InvoiceDto> getAllByIdUser(Integer idUser) {
        return invoiceRepository.findAllByIdUser(idUser)
                .stream()
                .map(invoiceTransform::invoiceToInvoiceDto)
                .collect(Collectors.toList());
    }

    public List<InvoiceDto> getAllByIdPet(Integer idPet) {
        return invoiceRepository.findAllByIdPet(idPet)
                .stream()
                .map(invoiceTransform::invoiceToInvoiceDto)
                .collect(Collectors.toList());
    }


    public InvoiceDto updateInvoice(InvoiceDto invoice) {
        try {
            Invoice invoiceDb = invoiceTransform.invoiceDtoToInvoice(invoice);
            invoiceDb.validateUpdate();
            return invoiceTransform.invoiceToInvoiceDto(invoiceRepository.save(invoiceDb));
        } catch (NullPointerException e) {
            throw new InvoiceException(messageUtil.resolveMessageSource("dto.null.exception.message"));

        }

    }

    public InvoiceDto getOneById(Long id) {
        if (null != id) {
            return invoiceTransform.invoiceToInvoiceDto(invoiceRepository.findById(id).orElse(null));
        }
        throw new MedicalExamException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }


    private List<DetailInvoiceDto> generatePDFInvoice(Integer id) {
        if (null != id) {
            return detailInvoiceService.getAllByidInvoice(id);
        }
        throw new InvoiceException(messageUtil.resolveMessageSource("fieldRequire.exception.message"));
    }

    private String getPDF(Long invo) {
        InvoiceDto invoiceF = getOneById(invo);
        Document document = new Document();
        String title = "Factura.pdf";

        try {
            PdfWriter.getInstance(document, new FileOutputStream(title));
            document.open();
            PdfPTable tableHeader = new PdfPTable(1);
            tableHeader.addCell("Fecha: " + invoiceF.getIdUser() + "\n" + "Mascota:" + invoiceF.getIdPet());

            PdfPTable tableContent = new PdfPTable(5);
            addTableHeader(tableContent);
            addRows(tableContent, invo.intValue());

            PdfPTable tableFooter = new PdfPTable(1);
            tableFooter.addCell("Valor: " + invoiceF.getTotalPrice() + "\n" + "Descuento: " + invoiceF.getDiscount() + "\n \n");

            document.add(tableHeader);
            document.add(tableContent);
            document.add(tableFooter);
            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return title;

    }

    private void addTableHeader(PdfPTable table) {
        if (null != table) {
            Stream.of("No. Factura", "gg", "Precio", "Servicio", "Colaboracor")
                    .forEach(columnTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        header.setBorderWidth(1);
                        header.setPhrase(new Phrase(columnTitle));
                        table.addCell(header);
                    });
        }
    }

    private void addRows(PdfPTable table, Integer invo) {
        if (null != table && null != invo) {
            List<DetailInvoiceDto> detailInvoice = generatePDFInvoice(invo);
            for (DetailInvoiceDto inv : detailInvoice) {
                String idInvoice = Integer.toString(inv.getIdInvoice());
                String value = Long.toString(inv.getQuantity());
                String price = Long.toString(inv.getPrice());
                String idService = Integer.toString(inv.getIdService());
                String idColaborator = Integer.toString(inv.getIdColaborator());
                table.addCell(idInvoice);
                table.addCell(value);
                table.addCell(price);
                table.addCell(idService);
                table.addCell(idColaborator);
            }
        }
    }

}

