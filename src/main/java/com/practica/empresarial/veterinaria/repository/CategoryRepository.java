package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    "id_categoria," +
                    "nombre_categoria, " +
                    "estado_categoria, " +
                    "descripcion_categoria " +
                    "FROM categoria " +
                    "WHERE nombre_categoria =:nom")
    Optional<Category> getOneByName(@Param(value = "nom") String nombre);
}
