package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.DetailInvoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetailInvoiceRepository extends JpaRepository<DetailInvoice, Long> {

    List<DetailInvoice> findAllByIdInvoice(Integer idInvoice);
}
