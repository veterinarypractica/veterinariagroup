package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.ExamDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamDetailRepository extends JpaRepository<ExamDetail, Long> {

    List<ExamDetail> findAllByIdExam(Long idExam);
}
