package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.UserVeterinary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserVeterinaryRepository extends JpaRepository<UserVeterinary, Long> {

    Optional<UserVeterinary> findByUserName(String userName);
}
