package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    List<Invoice> findAllByIdUser(Integer idUser);

    List<Invoice> findAllByIdPet(Integer idPet);

}
