package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.Income;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Long> {

}
