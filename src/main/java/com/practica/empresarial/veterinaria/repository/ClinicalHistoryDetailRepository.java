package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.ClinicalHistoryDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicalHistoryDetailRepository extends JpaRepository<ClinicalHistoryDetail, Long> {

}
