package com.practica.empresarial.veterinaria.repository;

import com.practica.empresarial.veterinaria.model.MedicalExam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalExamRepository extends JpaRepository<MedicalExam, Long> {

    List<MedicalExam> getAllByPet(Long pet);

    MedicalExam getOneByIdMedicalExam(Long id);
}
