package com.practica.empresarial.veterinaria.exceptions;

public class BranchOfficeException extends RuntimeException {

    public BranchOfficeException(String message) {
        super(message);
    }


}
