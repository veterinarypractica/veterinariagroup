package com.practica.empresarial.veterinaria.exceptions;

public class AuthException extends RuntimeException {


    public AuthException(String message) {
        super(message);
    }
}
