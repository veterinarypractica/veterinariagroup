package com.practica.empresarial.veterinaria.exceptions;

public class PetException extends RuntimeException {

    public PetException(String message) {
        super(message);
    }
}
