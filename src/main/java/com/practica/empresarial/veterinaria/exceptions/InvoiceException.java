package com.practica.empresarial.veterinaria.exceptions;

public class InvoiceException extends RuntimeException {

    public InvoiceException(String message) {
        super(message);
    }

}
