package com.practica.empresarial.veterinaria.exceptions;

public class ServiceVeterinaryException extends RuntimeException {

    public ServiceVeterinaryException(String message) {
        super(message);
    }


}
