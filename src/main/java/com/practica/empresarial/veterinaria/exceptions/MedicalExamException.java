package com.practica.empresarial.veterinaria.exceptions;

public class MedicalExamException extends RuntimeException {

    public MedicalExamException(String message) {
        super(message);
    }
}
