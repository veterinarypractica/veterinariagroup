package com.practica.empresarial.veterinaria.exceptions;

public class ClinicalHistoryDetailException extends RuntimeException {

    public ClinicalHistoryDetailException(String message) {
        super(message);
    }
}
