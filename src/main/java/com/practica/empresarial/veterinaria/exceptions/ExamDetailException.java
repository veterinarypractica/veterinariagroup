package com.practica.empresarial.veterinaria.exceptions;

public class ExamDetailException extends RuntimeException {
    public ExamDetailException(String message) {
        super(message);
    }
}
