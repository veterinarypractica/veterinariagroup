package com.practica.empresarial.veterinaria.exceptions;

public class ClinicalHistoryException extends RuntimeException {
    public ClinicalHistoryException(String message) {
        super(message);
    }

}
