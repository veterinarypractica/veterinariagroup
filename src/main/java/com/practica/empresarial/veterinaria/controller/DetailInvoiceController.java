package com.practica.empresarial.veterinaria.controller;


import com.practica.empresarial.veterinaria.model.DetailInvoice;
import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;
import com.practica.empresarial.veterinaria.service.DetailInvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/detailInvoice")
@Api("detailInvoice")
public class DetailInvoiceController {
    final DetailInvoiceService detailInvoiceService;

    @Autowired
    public DetailInvoiceController(DetailInvoiceService detailInvoiceService) {
        this.detailInvoiceService = detailInvoiceService;
    }

    @GetMapping(path = "/all/idfactura")
    @ApiOperation(value = "get all Detail Factura for idInvoice", response = DetailInvoice.class)
    public List<DetailInvoiceDto> getAllByIdInvoice(@RequestParam("idInvoice") Integer idInvoice) {
        return detailInvoiceService.getAllByidInvoice(idInvoice);
    }

}
