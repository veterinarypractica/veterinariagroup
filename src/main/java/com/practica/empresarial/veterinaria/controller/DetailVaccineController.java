package com.practica.empresarial.veterinaria.controller;


import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import com.practica.empresarial.veterinaria.service.DetailVaccineServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/detailvaccine")
@Api("detailvaccine")
public class DetailVaccineController {

    private final DetailVaccineServices detailVaccineServices;

    @Autowired
    public DetailVaccineController(DetailVaccineServices detailVaccineServices) {
        this.detailVaccineServices = detailVaccineServices;
    }

    @GetMapping(path = "/all/cardvaccine")
    @ApiOperation(value = "get card vaccine for idcardvaccine", response = DetailVaccineDto.class)
    public List<DetailVaccineDto> getAllByIdCardVaccine(@RequestParam("idcardvaccine") Integer idCardVaccine) {
        return detailVaccineServices.getAllByIdCardVaccine(idCardVaccine);
    }

}
