package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Income;
import com.practica.empresarial.veterinaria.model.dto.IncomeDto;
import com.practica.empresarial.veterinaria.service.IncomeServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/income")
@Api("income")
public class IncomeController {
    private final IncomeServices incomeServices;

    @Autowired
    public IncomeController(IncomeServices incomeServices) {
        this.incomeServices = incomeServices;
    }

    @GetMapping(path = "/allIncome")
    @ApiOperation(value = "get all income", response = Income.class)
    public List<IncomeDto> getAllIncome() {
        return incomeServices.getAllIncome();
    }

}
