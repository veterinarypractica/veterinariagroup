package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDto;
import com.practica.empresarial.veterinaria.service.ClinicalHistoryServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clinicalhistory")
@Api("clinicalhistory")
public class ClinicalHistoryController {

    private final ClinicalHistoryServices clinicalHistoryServices;

    @Autowired
    public ClinicalHistoryController(ClinicalHistoryServices clinicalHistoryServices) {
        this.clinicalHistoryServices = clinicalHistoryServices;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "create clinicalHistory", response = ClinicalHistoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ClinicalHistoryDto createClinicalHistory(@RequestBody ClinicalHistoryDto clinicalHistoryDto) {
        return clinicalHistoryServices.createClinicalHistory(clinicalHistoryDto);
    }
}
