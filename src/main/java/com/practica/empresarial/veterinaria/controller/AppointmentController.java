package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Appointment;
import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;
import com.practica.empresarial.veterinaria.service.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/appointment")
@Api("appointment")
public class AppointmentController {

    final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping(path = "/all/topic/vaccine")
    @ApiOperation(value = "get all appointment for topic vaccine", response = Appointment.class)
    public List<AppointmentDto> getAllByTopicAppointment(@RequestParam("topicVaccine") String topicAppointment) {
        return appointmentService.getAllByTopicAppointment(topicAppointment);
    }

}
