package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Invoice;
import com.practica.empresarial.veterinaria.model.dto.InvoiceDto;
import com.practica.empresarial.veterinaria.service.InvoiceServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/invoice")
@Api("invoice")
public class InvoiceContoller {
    final InvoiceServices invoiceService;

    @Autowired
    public InvoiceContoller(InvoiceServices invoiceService) {
        this.invoiceService = invoiceService;
    }

    /*@GetMapping(path = "/mail/id")
    @ApiOperation(value = "send mail Invoice for id", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public String sendMail(@RequestParam("id") Long id) {
        return invoiceService.sendEmailById(id);
    }*/

    @GetMapping(path = "/pdf/email")
    @ApiOperation(value = "send mail", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public String sendMailPDF(@RequestParam(name = "email") String email, @RequestParam(name = "idInvoice") Long idInvoice) {
        return invoiceService.sendEmailByIdIvnoice(email, idInvoice);
    }

    @GetMapping(path = "/all/idUser")
    @ApiOperation(value = "get all invoice by Iduser", response = Invoice.class)
    public List<InvoiceDto> getAllByIdUser(@RequestParam("idUser") Integer idUser) {
        return invoiceService.getAllByIdUser(idUser);
    }

    @GetMapping(path = "/all/idPet")
    @ApiOperation(value = "get all invoice by IdPet", response = Invoice.class)
    public List<InvoiceDto> getAllByIdPet(@RequestParam("idPet") Integer idPet) {
        return invoiceService.getAllByIdPet(idPet);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update Invoice", response = InvoiceDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public InvoiceDto updateInvoice(@RequestBody InvoiceDto invoice) {
        return invoiceService.updateInvoice(invoice);
    }


    @GetMapping(path = "/getInvoice")
    @ApiOperation(value = "get", response = InvoiceDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public InvoiceDto getOneById(@RequestParam("idInvoice") Long idInvoice) {
        return invoiceService.getOneById(idInvoice);
    }


}
