package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.BranchOffice;
import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;
import com.practica.empresarial.veterinaria.service.BranchOfficeServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branchOffice")
@Api("branchOffice")
public class BranchOfficeController {
    final BranchOfficeServices branchOfficeServices;

    @Autowired
    public BranchOfficeController(BranchOfficeServices branchOfficeServices) {
        this.branchOfficeServices = branchOfficeServices;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Registry branchOffice", response = BranchOfficeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public BranchOfficeDto createBranchOffice(@RequestBody BranchOfficeDto branchOffice) {
        return branchOfficeServices.createBranchOffice(branchOffice);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update branchOffice", response = BranchOfficeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public BranchOfficeDto updateBranchOffice(@RequestBody BranchOfficeDto branchOffice) {
        return branchOfficeServices.updateBranchOffice(branchOffice);

    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get branchOffice", response = BranchOffice.class)
    public List<BranchOfficeDto> getAll() {
        return branchOfficeServices.getAllBranchOffice();
    }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "delete Veterinary for id", response = BranchOffice.class)
    public String deleteById(@RequestParam(name = "id") Long id) {
        return branchOfficeServices.deleteBranchOffice(id);
    }

}
