package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.ServiceVeterinary;
import com.practica.empresarial.veterinaria.model.dto.ServiceVeterinaryDto;
import com.practica.empresarial.veterinaria.service.ServiceVeterinaryServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/service")
@Api("service")
public class ServiceVeterinaryController {
    final ServiceVeterinaryServices serviceVeterinaryServices;

    @Autowired
    public ServiceVeterinaryController(ServiceVeterinaryServices serviceVeterinaryServices) {
        this.serviceVeterinaryServices = serviceVeterinaryServices;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "create Service", response = ServiceVeterinary.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ServiceVeterinaryDto createServices(@RequestBody ServiceVeterinaryDto service) {
        return serviceVeterinaryServices.createServices(service);
    }

    @PutMapping(path = "/updates")
    @ApiOperation(value = "update Service", response = ServiceVeterinary.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ServiceVeterinaryDto updateServices(@RequestBody ServiceVeterinaryDto service) {
        return serviceVeterinaryServices.updateService(service);
    }

    @GetMapping(path = "/all/name")
    @ApiOperation(value = "get all Service for name", response = ServiceVeterinary.class)
    public List<ServiceVeterinaryDto> getAllServiceByNameService(@RequestParam("name") String nameService) {
        return serviceVeterinaryServices.getAllServiceByNameService(nameService);
    }

    @GetMapping(path = "/all/type")
    @ApiOperation(value = "get all Service for category", response = ServiceVeterinary.class)
    public List<ServiceVeterinaryDto> getAllServiceByType(@RequestParam("type") Integer idServiceVeterinary) {
        return serviceVeterinaryServices.getAllServiceByType(idServiceVeterinary);
    }

}
