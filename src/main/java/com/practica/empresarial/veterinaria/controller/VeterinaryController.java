package com.practica.empresarial.veterinaria.controller;

import com.practica.empresarial.veterinaria.model.Veterinary;
import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;
import com.practica.empresarial.veterinaria.service.VeterinaryServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/veterinary")
@Api("veterinary")
public class VeterinaryController {

    private final VeterinaryServices veterinaryServices;

    @Autowired
    public VeterinaryController(VeterinaryServices veterinaryServices) {
        this.veterinaryServices = veterinaryServices;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "create Veterinary", response = VeterinaryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public VeterinaryDto createVeterinary(@RequestBody VeterinaryDto veterinary) {
        return veterinaryServices.createVeterinary(veterinary);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update Veterinary", response = VeterinaryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public VeterinaryDto updateVeterinary(@RequestBody VeterinaryDto veterinary) {
        return veterinaryServices.updateVeterinary(veterinary);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "get All Veterinary", response = Veterinary.class)
    public List<VeterinaryDto> getAll() {
        return veterinaryServices.getAllVeterinary();
    }

    @GetMapping(path = "/all/name")
    @ApiOperation(value = "get All Veterinary for name", response = Veterinary.class)
    public List<VeterinaryDto> getAllByName(@RequestParam(name = "name") String name) {
        return veterinaryServices.getAllVeterinaryByName(name);
    }

    @DeleteMapping(path = "/delete/id")
    @ApiOperation(value = "delete all Veterinary for id", response = String.class)
    public String deleteById(@RequestParam(name = "id") Long id) {
        return veterinaryServices.deleteVeterinary(id);
    }

}
