package com.practica.empresarial.veterinaria.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class MessageUtil {

    @Autowired
    private MessageSource messageExceptionSourceSource;

    public String resolveMessageSource(String code) {
        return messageExceptionSourceSource.getMessage(code, null, Locale.getDefault());
    }
}
