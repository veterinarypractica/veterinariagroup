package com.practica.empresarial.veterinaria.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;

@Component
public class PdfUtil {

    private final MessageUtil messageUtil;

    @Autowired
    public PdfUtil(MessageUtil messageUtil) {
        this.messageUtil = messageUtil;
    }

    public String generatePdf(String title,
                              PdfPTable header,
                              PdfPTable content,
                              PdfPTable footer) {

        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(title));
            document.open();
            document.add(header);
            document.add(content);
            document.add(footer);
            document.close();
            return title;
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            throw new VeterinaryException(messageUtil.resolveMessageSource("failed.generate.pdf"));
        }

    }

    public void addTableTitle(PdfPTable table, String[] titles) {

        if (null != table) {
            Arrays.asList(titles)
                    .forEach(columnTitle -> {
                        table.addCell(getHeaderTitle(columnTitle));
                    });
        }
    }

    @NotNull
    private PdfPCell getHeaderTitle(String columnTitle) {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(1);
        header.setPhrase(new Phrase(columnTitle));
        return header;
    }


}
