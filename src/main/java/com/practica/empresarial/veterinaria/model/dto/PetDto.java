package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PetDto {

    private Long idPet;
    private String name;
    private String species;
    private Integer age;
    private String race;
    private String sex;
    private String observations;
    private Integer idUser;
    private Boolean status;
}
