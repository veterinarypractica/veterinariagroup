package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.Category;
import com.practica.empresarial.veterinaria.model.dto.CategoryDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryTransform {


    public Category categoryDtoToCategory(CategoryDto category) {
        Category categoryPersist = null;
        if (category != null) {
            categoryPersist = Category.builder()
                    .idCategory(category.getIdCategory())
                    .description(category.getDescriptionCategory())
                    .status(category.getStatusCategory())
                    .name(category.getNameCategory())
                    .build();
        }
        return categoryPersist;

    }

    public CategoryDto categoryToCategoryDto(Category category) {
        CategoryDto newCategoryDto = null;

        if (null != category) {
            newCategoryDto = CategoryDto.builder()
                    .idCategory(category.getIdCategory())
                    .nameCategory(category.getName())
                    .descriptionCategory(category.getDescription())
                    .statusCategory(category.getStatus())
                    .build();
        }
        return newCategoryDto;
    }

}
