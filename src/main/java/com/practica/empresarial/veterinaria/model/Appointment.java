package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cita")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cita")
    private Long idAppointment;

    @Column(name = "fecha_hora_cita")
    private Timestamp dateTimeAppointment;

    @Column(name = "estado_cita")
    private Boolean stateAppointment;

    @Column(name = "tema_cita")
    private String topicAppointment;

    @Column(name = "id_mascota")
    private Integer idPet;

}
