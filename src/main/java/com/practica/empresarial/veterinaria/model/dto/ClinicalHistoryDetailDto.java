package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class ClinicalHistoryDetailDto {

    private Long idDetailHC;
    private Float temperature;
    private Float weight;
    private Float heartRate;
    private Float breathingFrequency;
    private Date admissionDate;
    private Long idClinicalHistory;
    private String feeding;
    private Date dewormingDate;
    private String habitat;
    private Float capillaryTime;
    private String reproductiveStatus;
    private String observations;
    private Long idCollaborator;
}
