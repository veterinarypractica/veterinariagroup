package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.Appointment;
import com.practica.empresarial.veterinaria.model.dto.AppointmentDto;
import org.springframework.stereotype.Component;

@Component
public class AppointmentTransform {

    public AppointmentDto appointmentToAppointmentDto(Appointment appointment) {
        AppointmentDto newAppointmentDto = null;
        if (appointment != null) {
            newAppointmentDto = AppointmentDto.builder()
                    .idAppointment(appointment.getIdAppointment())
                    .dateTimeAppointment(appointment.getDateTimeAppointment())
                    .stateAppointment(appointment.getStateAppointment())
                    .topicAppointment(appointment.getTopicAppointment())
                    .idPet(appointment.getIdPet())
                    .build();
        }
        return newAppointmentDto;
    }
}