package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detalle_vacuna")
public class DetailVaccine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_vacuna")
    private Long idDetailVaccine;

    @Column(name = "nombre_vacuna")
    private String nameVaccine;

    @Column(name = "dosis_vacuna")
    private Float doseVaccine;

    @Column(name = "fecha_vacuna")
    private Date dateVaccine;

    @Column(name = "codigo_vacuna")
    private Float codeVaccine;

    @Column(name = "id_carne_vacuna")
    private Integer idCardVaccine;

    @Column(name = "estado")
    private Boolean state;
}
