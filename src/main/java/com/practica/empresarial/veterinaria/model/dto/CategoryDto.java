package com.practica.empresarial.veterinaria.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {

    private Long idCategory;
    private String nameCategory;
    private Boolean statusCategory;
    private String descriptionCategory;

}
