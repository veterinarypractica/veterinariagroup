package com.practica.empresarial.veterinaria.model.dto;


import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Builder
@Data
public class AppointmentDto {

    private Long idAppointment;
    private Timestamp dateTimeAppointment;
    private Boolean stateAppointment;
    private String topicAppointment;
    private Integer idPet;
}
