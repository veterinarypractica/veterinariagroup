package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.BranchOfficeException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Builder
@Getter
@AllArgsConstructor
@Entity
@Table(name = "sucursal", schema = "public")
public class BranchOffice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sucursal")
    private Long idBranchOffice;

    @Column(name = "telefono_sucursal")
    private Long branchPhone;

    @Column(name = "direccion_sucursal")
    private String address;

    @Column(name = "logo_sucursal")
    private String logo;

    @Column(name = "admin_sucursal")
    private Long admin;

    @Column(name = "veterinaria")
    private Long idVeterinaria;

    public void validateCreate() {
        if (idBranchOffice != null) {
            throw new BranchOfficeException("campo id erroneo");
        }
        validateField();
    }

    public void validateUpdate() {
        if (this.idBranchOffice == null) {
            throw new BranchOfficeException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {

        validateFieldNotEmpty();

        validateValueField();

    }

    private void validateFieldNotEmpty() {

        String fieldsEmpty = "los campos están vacios";
        if (StringUtils.isBlank(address)) {
            throw new BranchOfficeException(fieldsEmpty);
        }
        if (StringUtils.isBlank(logo)) {
            throw new BranchOfficeException(fieldsEmpty);
        }
        if (null == branchPhone) {
            throw new BranchOfficeException(fieldsEmpty);
        }
        if (null == admin) {
            throw new BranchOfficeException(fieldsEmpty);
        }
        if (null == idVeterinaria) {
            throw new BranchOfficeException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (admin <= 0f) {
            throw new BranchOfficeException(valueInvalid);
        }
        if (branchPhone <= 0f) {
            throw new BranchOfficeException(valueInvalid);
        }
        if (idVeterinaria <= 0f) {
            throw new BranchOfficeException(valueInvalid);
        }
    }
}
