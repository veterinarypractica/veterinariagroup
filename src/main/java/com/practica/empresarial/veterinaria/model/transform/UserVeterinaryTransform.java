package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.UserVeterinary;
import com.practica.empresarial.veterinaria.model.dto.UserVeterinaryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserVeterinaryTransform {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserVeterinaryDto userToUserDto(UserVeterinary userVeterinary) {

        UserVeterinaryDto userVeterinaryDto = null;
        if (userVeterinary != null) {
            userVeterinaryDto = UserVeterinaryDto.builder()
                    .idUser(userVeterinary.getIdUser())
                    .name(userVeterinary.getName())
                    .lastName(userVeterinary.getLastName())
                    .documentType(userVeterinary.getDocumentType())
                    .documentNumber(userVeterinary.getDocumentNumber())
                    .birthDate(userVeterinary.getBirthDate())
                    .department(userVeterinary.getDepartment())
                    .city(userVeterinary.getCity())
                    .neighborhood(userVeterinary.getNeighborhood())
                    .phone(userVeterinary.getPhone())
                    .email(userVeterinary.getEmail())
                    .userName(userVeterinary.getUserName())
                    .pass(userVeterinary.getPass())
                    .status(userVeterinary.getStatus())
                    .rol(userVeterinary.getRol())
                    .token(userVeterinary.getToken())
                    .build();
        }
        return userVeterinaryDto;
    }

    public UserVeterinary userDtoToUser(UserVeterinaryDto userVeterinaryDto) {
        UserVeterinary userVeterinary = null;
        if (userVeterinaryDto != null) {
            userVeterinary = UserVeterinary.builder()
                    .idUser(userVeterinaryDto.getIdUser())
                    .name(userVeterinaryDto.getName())
                    .lastName(userVeterinaryDto.getLastName())
                    .documentType(userVeterinaryDto.getDocumentType())
                    .documentNumber(userVeterinaryDto.getDocumentNumber())
                    .birthDate(userVeterinaryDto.getBirthDate())
                    .department(userVeterinaryDto.getDepartment())
                    .city(userVeterinaryDto.getCity())
                    .neighborhood(userVeterinaryDto.getNeighborhood())
                    .phone(userVeterinaryDto.getPhone())
                    .email(userVeterinaryDto.getEmail())
                    .userName(userVeterinaryDto.getUserName())
                    .pass(bCryptPasswordEncoder.encode(userVeterinaryDto.getPass()))
                    .status(userVeterinaryDto.getStatus())
                    .rol(userVeterinaryDto.getRol())
                    .token(userVeterinaryDto.getToken())
                    .build();
        }
        return userVeterinary;
    }
}
