package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.VeterinaryException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "veterinaria")
public class Veterinary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_veterinaria")
    private Long idVeterinary;

    @Column(name = "nombre_veterinaria")
    private String name;

    @Column(name = "nit_veterinaria")
    private Long nit;

    @Column(name = "correo_veterinaria")
    private String email;

    @Column(name = "admin_veterinaria")
    private Long owner;

    @Column(name = "estado_veterinaria")
    private Boolean status;

    public void validateCreate() {
        if (null != idVeterinary) {
            throw new VeterinaryException("campo id erroneo");
        }
        validateField();
    }

    public void validateUpdate() {
        if (null == this.idVeterinary) {
            throw new VeterinaryException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {
        validateFieldNotEmpty();
        validateValueField();

    }

    private void validateFieldNotEmpty() {

        String fieldsEmpty = "los campos están vacios";
        if (StringUtils.isBlank(name)) {
            throw new VeterinaryException(fieldsEmpty);
        }
        if (StringUtils.isBlank(email)) {
            throw new VeterinaryException(fieldsEmpty);
        }
        if (null == nit) {
            throw new VeterinaryException(fieldsEmpty);
        }
        if (null == owner) {
            throw new VeterinaryException(fieldsEmpty);
        }
        if (null == status) {
            throw new VeterinaryException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (owner <= 0f) {
            throw new VeterinaryException(valueInvalid);
        }
        if (nit <= 0f) {
            throw new VeterinaryException(valueInvalid);
        }
    }
}
