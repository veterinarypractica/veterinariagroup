package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.CardVaccine;
import com.practica.empresarial.veterinaria.model.dto.CardVaccineDto;
import org.springframework.stereotype.Component;

@Component
public class CardVaccineTransform {

    public CardVaccineDto cardVaccineToCardVaccineDto(CardVaccine cardVaccine) {
        CardVaccineDto newCardVaccineDto = null;
        if (cardVaccine != null) {
            newCardVaccineDto = CardVaccineDto.builder()
                    .idCardVaccine(cardVaccine.getIdCardVaccine())
                    .idPet(cardVaccine.getIdPet())
                    .build();
        }
        return newCardVaccineDto;
    }
}
