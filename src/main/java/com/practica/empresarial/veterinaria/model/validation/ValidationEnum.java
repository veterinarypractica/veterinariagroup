package com.practica.empresarial.veterinaria.model.validation;

public enum ValidationEnum {

    CHARTERS_NUMERICS("1234567890"),
    CHARTERS_SPECIALS("|@&$</>");

    private final String value;

    ValidationEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
