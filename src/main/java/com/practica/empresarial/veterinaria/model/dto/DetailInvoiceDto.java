package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DetailInvoiceDto {

    private Long idDetail;
    private Long quantity;
    private Long price;
    private Integer idService;
    private Integer idInvoice;
    ;
    private Integer idColaborator;

}
