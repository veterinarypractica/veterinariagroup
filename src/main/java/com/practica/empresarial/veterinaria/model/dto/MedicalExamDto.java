package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MedicalExamDto {

    private Long idMedicalExam;
    private Long type;
    private Long pet;
    private String observation;
    private String result;
}
