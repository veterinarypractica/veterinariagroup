package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class VeterinaryDto {

    private Long idVeterinary;
    private String name;
    private Long nit;
    private String email;
    private Long owner;
    private Boolean status;
}
