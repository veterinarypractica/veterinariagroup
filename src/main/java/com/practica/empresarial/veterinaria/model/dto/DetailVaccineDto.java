package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class DetailVaccineDto {

    private Long idDetailVaccine;
    private String nameVaccine;
    private Float doseVaccine;
    private Date dateVaccine;
    private Float codeVaccine;
    private Integer idCardVaccine;
    private Boolean state;
}
