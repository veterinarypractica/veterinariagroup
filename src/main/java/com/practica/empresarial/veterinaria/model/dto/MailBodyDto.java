package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MailBodyDto {

    private String email;
    private String content;
    private String subject;
}
