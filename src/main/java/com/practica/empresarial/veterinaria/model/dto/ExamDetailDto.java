package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExamDetailDto {

    private Long idDetailExam;
    private String name;
    private String value;
    private Long idExam;
}
