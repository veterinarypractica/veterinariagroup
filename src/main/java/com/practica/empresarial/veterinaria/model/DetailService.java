package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.InvoiceException;
import lombok.*;

import javax.persistence.*;


@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Detalle_factura")
public class DetailService {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle")
    private Long idDetail;

    @Column(name = "cantidad")
    private Long quantity;

    @Column(name = "precio")
    private Long price;

    @Column(name = "id_servicio")
    private Long idService;

    @Column(name = "id_factura")
    private Long idInvoice;

    @Column(name = "id_colaborador")
    private Long idCollaborator;


    public void validateCreate() {
        if (null != idDetail) {
            throw new InvoiceException("campo id erroneo");
        }
    }
}
