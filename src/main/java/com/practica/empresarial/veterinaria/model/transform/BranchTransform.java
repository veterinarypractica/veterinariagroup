package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.BranchOffice;
import com.practica.empresarial.veterinaria.model.dto.BranchOfficeDto;
import org.springframework.stereotype.Component;

@Component
public class BranchTransform {

    public BranchOffice branchDtoToBranch(BranchOfficeDto branchDto) {
        BranchOffice branchOffice = null;
        if (branchDto != null) {
            branchOffice = BranchOffice.builder()
                    .idBranchOffice(branchDto.getIdBranchOffice())
                    .address(branchDto.getAddress())
                    .branchPhone(branchDto.getBranchPhone())
                    .logo(branchDto.getLogo())
                    .idVeterinaria(branchDto.getIdVeterinary())
                    .admin(branchDto.getAdmin())
                    .build();
        }
        return branchOffice;
    }

    public BranchOfficeDto branchToBranchDto(BranchOffice branchOffice) {
        BranchOfficeDto branchOfficeDto = null;
        if (branchOffice != null) {
            branchOfficeDto = BranchOfficeDto.builder()
                    .idBranchOffice(branchOffice.getIdBranchOffice())
                    .address(branchOffice.getAddress())
                    .branchPhone(branchOffice.getBranchPhone())
                    .logo(branchOffice.getLogo())
                    .idVeterinary(branchOffice.getIdVeterinaria())
                    .admin(branchOffice.getAdmin())
                    .build();
        }
        return branchOfficeDto;
    }
}
