package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.InvoiceException;
import lombok.*;

import javax.persistence.*;


@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detalle_factura")
public class DetailInvoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle")
    private Long idDetail;

    @Column(name = "cantidad")
    private Long quantity;

    @Column(name = "precio")
    private Long price;

    @Column(name = "id_servicio")
    private Integer idService;

    @Column(name = "id_factura")
    private Integer idInvoice;
    ;

    @Column(name = "id_colaborador")
    private Integer idColaborator;


    public void validateCreate() {
        if (null != idDetail) {
            throw new InvoiceException("campo id erroneo");
        }
//        validateCreate();
    }
}
