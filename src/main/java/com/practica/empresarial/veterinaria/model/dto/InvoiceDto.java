package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class InvoiceDto {

    private Long idInvoice;
    private Date dateInvoice;
    private Float value;
    private Float discount;
    private Float totalPrice;
    private Integer idUser;
    private Integer idBranch;
    private Integer idPet;
    private Boolean status;

}
