package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.DetailInvoice;
import com.practica.empresarial.veterinaria.model.dto.DetailInvoiceDto;
import org.springframework.stereotype.Component;

@Component
public class DetailInvoiceTransform {

    public DetailInvoice detailInvoiceDtoToDetailInvoice(DetailInvoiceDto detailInvoice) {
        DetailInvoice detailInvoicePersist = null;
        if (detailInvoice != null) {
            detailInvoicePersist = DetailInvoice.builder()
                    .idDetail(detailInvoice.getIdDetail())
                    .quantity(detailInvoice.getQuantity())
                    .price(detailInvoice.getPrice())
                    .idService(detailInvoice.getIdService())
                    .idInvoice(detailInvoice.getIdInvoice())
                    .idColaborator(detailInvoice.getIdColaborator())
                    .build();
        }
        return detailInvoicePersist;

    }


    public DetailInvoiceDto detailInvoiceToDetailInvoiceDto(DetailInvoice detailInvoice) {
        DetailInvoiceDto NewDetailInvoiceDto = null;
        if (detailInvoice != null) {
            NewDetailInvoiceDto = DetailInvoiceDto.builder()
                    .idDetail(detailInvoice.getIdDetail())
                    .quantity(detailInvoice.getQuantity())
                    .price(detailInvoice.getPrice())
                    .idService(detailInvoice.getIdService())
                    .idInvoice(detailInvoice.getIdInvoice())
                    .idColaborator(detailInvoice.getIdColaborator())
                    .build();
        }
        return NewDetailInvoiceDto;
    }
}
