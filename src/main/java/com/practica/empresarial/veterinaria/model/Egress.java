package com.practica.empresarial.veterinaria.model;

import com.practica.empresarial.veterinaria.exceptions.EgressException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "egreso")
public class Egress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_egreso")
    private Long idEgress;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha_egreso")
    private Date date;

    @Column(name = "tipo_egreso")
    private String type;

    @Column(name = "valor_egreso")
    private Float value;

    @Column(name = "concepto_egreso")
    private String concept;

    @Column(name = "descripcion_egreso")
    private String description;

    @Column(name = "id_usuario")
    private Long idUser;

    @Column(name = "id_mascota")
    private Long idPet;

    public void validateCreate() {
        if (null != idEgress) {
            throw new EgressException("campo id erroneo");
        }
        validateField();

    }

    public void validateUpdate() {
        if (null == this.idEgress) {
            throw new EgressException("campo id erroneo");
        }
        validateField();
    }

    private void validateField() {

        validateFieldNotEmpty();
        validateValueField();
    }

    private void validateFieldNotEmpty() {

        String fieldsEmpty = "los campos están vacios";
        if (StringUtils.isBlank(description)) {
            throw new EgressException(fieldsEmpty);
        }
        if (StringUtils.isBlank(concept)) {
            throw new EgressException(fieldsEmpty);
        }
        if (StringUtils.isBlank(type)) {
            throw new EgressException(fieldsEmpty);
        }
        if (null == date) {
            throw new EgressException(fieldsEmpty);
        }
        if (null == value) {
            throw new EgressException(fieldsEmpty);
        }
    }

    private void validateValueField() {

        String valueInvalid = "el valor ingresado no esta permitido";
        if (value <= 0f) {
            throw new EgressException(valueInvalid);
        }
    }

}