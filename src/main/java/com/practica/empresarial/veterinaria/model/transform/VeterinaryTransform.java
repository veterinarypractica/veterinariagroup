package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.Veterinary;
import com.practica.empresarial.veterinaria.model.dto.VeterinaryDto;
import org.springframework.stereotype.Component;

@Component
public class VeterinaryTransform {

    public Veterinary veterinaryDtoToVeterinary(VeterinaryDto veterinaryDto) {
        Veterinary veterinaryPersist = null;
        if (veterinaryDto != null) {
            veterinaryPersist = Veterinary.builder()
                    .idVeterinary(veterinaryDto.getIdVeterinary())
                    .name(veterinaryDto.getName())
                    .email(veterinaryDto.getEmail())
                    .nit(veterinaryDto.getNit())
                    .owner(veterinaryDto.getOwner())
                    .status(veterinaryDto.getStatus())
                    .build();
        }
        return veterinaryPersist;
    }

    public VeterinaryDto veterinaryToVeterinaryDto(Veterinary veterinary) {
        VeterinaryDto veterinaryDto = null;
        if (veterinary != null) {
            veterinaryDto = VeterinaryDto.builder()
                    .idVeterinary(veterinary.getIdVeterinary())
                    .name(veterinary.getName())
                    .email(veterinary.getEmail())
                    .nit(veterinary.getNit())
                    .owner(veterinary.getOwner())
                    .status(veterinary.getStatus())
                    .build();
        }
        return veterinaryDto;
    }
}
