package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.ClinicalHistoryDetail;
import com.practica.empresarial.veterinaria.model.dto.ClinicalHistoryDetailDto;
import org.springframework.stereotype.Component;

@Component
public class ClinicalHistoryDetailTransform {

    public ClinicalHistoryDetail clinicalHistoryDtoToClinicalHistory(ClinicalHistoryDetailDto clinicalDto) {
        ClinicalHistoryDetail clinicalHistoryDetail = null;
        if (clinicalDto != null) {
            clinicalHistoryDetail = ClinicalHistoryDetail.builder()
                    .idDetailHC(clinicalDto.getIdDetailHC())
                    .temperature(clinicalDto.getTemperature())
                    .weight(clinicalDto.getWeight())
                    .heartRate(clinicalDto.getHeartRate())
                    .breathingFrequency(clinicalDto.getBreathingFrequency())
                    .admissionDate(clinicalDto.getAdmissionDate())
                    .idClinicalHistory(clinicalDto.getIdClinicalHistory())
                    .feeding(clinicalDto.getFeeding())
                    .dewormingDate(clinicalDto.getDewormingDate())
                    .habitat(clinicalDto.getHabitat())
                    .capillaryTime(clinicalDto.getCapillaryTime())
                    .reproductiveStatus(clinicalDto.getReproductiveStatus())
                    .observations(clinicalDto.getObservations())
                    .idCollaborator(clinicalDto.getIdCollaborator())
                    .build();


        }
        return clinicalHistoryDetail;

    }

    public ClinicalHistoryDetailDto clinicalHistoryToClinicalHistoryDto(ClinicalHistoryDetail clinical) {
        ClinicalHistoryDetailDto clinicalHistory = null;
        if (clinical != null) {
            clinicalHistory = ClinicalHistoryDetailDto.builder()
                    .idDetailHC(clinical.getIdDetailHC())
                    .temperature(clinical.getTemperature())
                    .weight(clinical.getWeight())
                    .heartRate(clinical.getHeartRate())
                    .breathingFrequency(clinical.getBreathingFrequency())
                    .admissionDate(clinical.getAdmissionDate())
                    .idClinicalHistory(clinical.getIdClinicalHistory())
                    .feeding(clinical.getFeeding())
                    .dewormingDate(clinical.getDewormingDate())
                    .habitat(clinical.getHabitat())
                    .capillaryTime(clinical.getCapillaryTime())
                    .reproductiveStatus(clinical.getReproductiveStatus())
                    .observations(clinical.getObservations())
                    .idCollaborator(clinical.getIdCollaborator())
                    .build();
        }
        return clinicalHistory;
    }
}
