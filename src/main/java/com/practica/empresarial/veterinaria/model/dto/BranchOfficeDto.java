package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BranchOfficeDto {

    private Long idBranchOffice;
    private Long branchPhone;
    private String address;
    private String logo;
    private Long admin;
    private Long idVeterinary;
}
