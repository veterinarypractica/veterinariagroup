package com.practica.empresarial.veterinaria.model.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class ClinicalHistoryDto {

    private Long idClinicalHistory;
    private Long idPet;
    private Date dateCreate;
}
