package com.practica.empresarial.veterinaria.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "ingreso")
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ingreso")
    private Long idIncome;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "fecha_ingreso")
    private Date date;

    @Column(name = "tipo_ingreso")
    private String type;

    @Column(name = "concepto_ingreso")
    private String concept;

    @Column(name = "id_sucursal")
    private Long branchOffice;

    @Column(name = "id_detalle_factura")
    private Long detailInvoice;
}
