package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.Income;
import com.practica.empresarial.veterinaria.model.dto.IncomeDto;
import org.springframework.stereotype.Component;

@Component
public class IncomeTransform {

    public IncomeDto incomeToIncomeDto(Income income) {
        IncomeDto newIncomeDto = null;
        if (income != null) {
            newIncomeDto = IncomeDto.builder()
                    .idIncome(income.getIdIncome())
                    .concept(income.getConcept())
                    .date(income.getDate())
                    .type(income.getType())
                    .branchOffice(income.getBranchOffice())
                    .build();
        }
        return newIncomeDto;
    }

}
