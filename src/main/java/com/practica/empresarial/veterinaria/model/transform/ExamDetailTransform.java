package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.ExamDetail;
import com.practica.empresarial.veterinaria.model.dto.ExamDetailDto;
import org.springframework.stereotype.Component;

@Component
public class ExamDetailTransform {
    public ExamDetailDto examDetailToExamDetailDto(ExamDetail examDetail) {
        ExamDetailDto newExamDetailDto = null;
        if (null != examDetail) {
            newExamDetailDto = ExamDetailDto.builder()
                    .idDetailExam(examDetail.getIdDetailExam())
                    .name(examDetail.getName())
                    .value(examDetail.getValue())
                    .idExam(examDetail.getIdExam())
                    .build();
        }
        return newExamDetailDto;
    }

}
