package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.MedicalExam;
import com.practica.empresarial.veterinaria.model.dto.MedicalExamDto;
import org.springframework.stereotype.Component;

@Component
public class MedicalExamTransform {
    public MedicalExamDto medicalExamToMedicalExamDto(MedicalExam medicalExam) {
        MedicalExamDto newMedicalExamDto = null;
        if (null != medicalExam) {
            newMedicalExamDto = MedicalExamDto.builder()
                    .idMedicalExam(medicalExam.getIdMedicalExam())
                    .type(medicalExam.getType())
                    .pet(medicalExam.getPet())
                    .observation(medicalExam.getObservation())
                    .result(medicalExam.getResult())
                    .build();
        }
        return newMedicalExamDto;
    }
}
