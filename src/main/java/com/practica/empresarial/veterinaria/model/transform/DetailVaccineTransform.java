package com.practica.empresarial.veterinaria.model.transform;

import com.practica.empresarial.veterinaria.model.DetailVaccine;
import com.practica.empresarial.veterinaria.model.dto.DetailVaccineDto;
import org.springframework.stereotype.Component;

@Component
public class DetailVaccineTransform {

    public DetailVaccineDto detailVaccineToDetailVaccineDto(DetailVaccine detailVaccine) {
        DetailVaccineDto newDetailVaccineDto = null;
        if (detailVaccine != null) {
            newDetailVaccineDto = DetailVaccineDto.builder()
                    .idDetailVaccine(detailVaccine.getIdDetailVaccine())
                    .nameVaccine(detailVaccine.getNameVaccine())
                    .doseVaccine(detailVaccine.getDoseVaccine())
                    .dateVaccine(detailVaccine.getDateVaccine())
                    .codeVaccine(detailVaccine.getCodeVaccine())
                    .idCardVaccine(detailVaccine.getIdCardVaccine())
                    .state(detailVaccine.getState())
                    .build();
        }
        return newDetailVaccineDto;
    }
}
